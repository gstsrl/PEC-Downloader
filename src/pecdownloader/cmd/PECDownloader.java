/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pecdownloader.cmd;

import it.sgsbp.application.commons.IDGen.IDGenerator;
import it.sgsbp.application.commons.configuration.ConfigLoader;
import it.sgsbp.application.commons.configuration.ConfigLoaderException;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.log4j.NDC;
import pecdownloader.engine.EngineEP;
import pecdownloader.engine.EngineException;

/**
 *
 * @author Riccardo
 */
public class PECDownloader {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
//System.setProperty("http.proxyHost", "10.209.4.34");
//System.setProperty("http.proxyPort", "8080");
        try {
            ConfigLoader.loadConfiguration();
        } catch (ConfigLoaderException e1) {
            System.exit(8);
        }
        String uniqueLoggerName = "";
        try {
            uniqueLoggerName = ConfigLoader.getProperties("LOGGER_NAME");
        } catch (ConfigLoaderException ex) {
            System.exit(8);
        }
        String id = IDGenerator.getID();
        MDC.put("IDUnivoco", id);
        NDC.push(PECDownloader.class.getCanonicalName());
        Logger log = Logger.getLogger(uniqueLoggerName);
        log.info("App Start");


        EngineEP eep = null;
        try {
            eep = new EngineEP();
        } catch (EngineException ex) {
            log.error(ex.toString());
        }
        if (eep != null) {
            eep.run();
        }
        
        log.info("App End");
        NDC.pop();
    }
}
