/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pecdownloader.engine.settings;

/**
 *
 * @author Riccardo
 */
public interface PECSettingsInterface {
    
    
    
    public void setPassword(String Password);
    public String getPassword( );
    public void setUsername(String Username);
    public String getUsername( );
    public void setServer(String Server);
    public String getServer( );
    public void setPort(String Port);
    public String getPort( );
    public void setFolder(String Folder);
    public String getFolder( );
    public void setProto(String Proto);
    public String getProto( );
    public void setPECUid(String PECUid);
    public String getPECUid( );
    
    
    
}
