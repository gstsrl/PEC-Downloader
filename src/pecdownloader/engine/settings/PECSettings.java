/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pecdownloader.engine.settings;

/**
 *
 * @author es03645
 */
public abstract class PECSettings implements PECSettingsInterface {
    
    protected String Server;
    protected String Port;
    protected String Username;
    protected String Password;
    protected String Proto;
    protected String Folder;
    private String PECUid;

    
    /**
     * @return the Server
     */
    @Override
    public String getServer() {
        return Server;
    }

    /**
     * @param Server the Server to set
     */
    @Override
    public void setServer(String Server) {
        this.Server = Server;
    }

    /**
     * @return the Username
     */
    @Override
    public String getUsername() {
        return Username;
    }

    /**
     * @param Username the Username to set
     */
    @Override
    public void setUsername(String Username) {
        this.Username = Username;
    }

    /**
     * @return the Password
     */
    @Override
    public String getPassword() {
        return Password;
    }

    /**
     * @param Password the Password to set
     */
    @Override
    public void setPassword(String Password) {
        this.Password = Password;
    }

    /**
     * @return the Proto
     */
    @Override
    public String getProto() {
        return Proto;
    }

    /**
     * @param Proto the Proto to set
     */
    @Override
    public void setProto(String Proto) {
        this.Proto = Proto;
    }

    /**
     * @return the Folder
     */
    @Override
    public String getFolder() {
        return Folder;
    }

    /**
     * @param Folder the Folder to set
     */
    @Override
    public void setFolder(String Folder) {
        this.Folder = Folder;
    }

    /**
     * @return the Port
     */
    @Override
    public String getPort() {
        return Port;
    }

    /**
     * @param Port the Port to set
     */
    @Override
    public void setPort(String Port) {
        this.Port = Port;
    }

    /**
     * @return the PECUid
     */
    @Override
    public String getPECUid() {
        return PECUid;
    }

    /**
     * @param PECUid the PECUid to set
     */
    @Override
    public void setPECUid(String PECUid) {
        this.PECUid = PECUid;
    }
    
    
  }
