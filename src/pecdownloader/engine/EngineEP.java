/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pecdownloader.engine;

import pecdownloader.engine.settings.PECSettings;
import it.sgsbp.application.commons.configuration.ConfigLoader;
import it.sgsbp.application.commons.configuration.ConfigLoaderException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.NDC;
import pecdownloader.engine.settings.IMAPPECSettings;
import pecdownloader.engine.settings.POPPECSettings;

/**
 *
 * @author Riccardo
 */
public class EngineEP {
    
    private int NTHREDS;
    private final String[] PEC_LIST;
    private Logger log;
    
    public EngineEP() throws EngineException {
        NDC.push(this.getClass().getCanonicalName());
        try {
            
            String uniqueLoggerName = ConfigLoader.getProperties("LOGGER_NAME");
            log = Logger.getLogger(uniqueLoggerName);
            log.info("Creazione Entry point di Engine");
            NTHREDS = NumberUtils.toInt(ConfigLoader.getProperties("NTHREDS"), 1);
            PEC_LIST = StringUtils.split(ConfigLoader.getProperties("PIPED_PECS"), "|");
            log.debug("NTHREDS:" + NTHREDS);
            log.debug("PEC_LIST:" + StringUtils.join(PEC_LIST, ";"));
        } catch (ConfigLoaderException ex) {
            throw new EngineException(ex.toString());
        }
    }
    
    public void run() {
        log.info("Avvio Motore Applicativo");
        ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
        //List<Future<Long>> list = new ArrayList<>();
        HashMap<String, Future<Long>> list = new HashMap<>();
        log.info("Crazione thread");
        for (String pec : PEC_LIST) {
            try {
                log.info("TH: " + pec);
                //Creo l'oggetto PEC-SETTING il quale contiene tutti i dati da 
                // passare al TH per operare sulla specifica casella
                String tmp = ConfigLoader.getProperties(pec, "RCV_PROTO");
                log.info("Tipologia di accesso alla casella: " + tmp);
                
                PECSettings ps = null;
                if (StringUtils.equals(tmp, "POP")) {
                    log.info("Configurazioni POP");
                    ps = new POPPECSettings();
                    ps.setPECUid(pec);
                    tmp = ConfigLoader.getProperties(pec, "POP");
                    log.debug(tmp);
                    ps.setServer(tmp);
                    tmp = ConfigLoader.getProperties(pec, "POP_PORT");
                    log.debug(tmp);
                    ps.setPort(tmp);
                    tmp = ConfigLoader.getProperties(pec, "POP_USER");
                    log.debug(tmp);
                    ps.setUsername(tmp);
                    tmp = ConfigLoader.getProperties(pec, "POP_PSW");
                    log.debug(tmp);
                    ps.setPassword(tmp);
                    tmp = ConfigLoader.getProperties(pec, "POP_PROTO");
                    log.debug(tmp);
                    ps.setProto(tmp);
                    ps.setFolder("INBOX");
                } else if (StringUtils.equals(tmp, "IMAP")) {
                    log.info("Configurazioni IMAP");
                    ps = new IMAPPECSettings();
                    ps.setPECUid(pec);
                    tmp = ConfigLoader.getProperties(pec, "IMAP");
                    log.debug(tmp);
                    ps.setServer(tmp);
                    tmp = ConfigLoader.getProperties(pec, "IMAP_PORT");
                    log.debug(tmp);
                    ps.setPort(tmp);
                    tmp = ConfigLoader.getProperties(pec, "IMAP_USER");
                    log.debug(tmp);
                    ps.setUsername(tmp);
                    tmp = ConfigLoader.getProperties(pec, "IMAP_PSW");
                    log.debug(tmp);
                    ps.setPassword(tmp);
                    tmp = ConfigLoader.getProperties(pec, "IMAP_PROTO");
                    log.debug(tmp);
                    ps.setProto(tmp);
                    tmp=ConfigLoader.getProperties(pec, "IMAP_DWN_FLD");
                    log.debug(tmp);
                    ps.setFolder(tmp);
                    
                }
                //Il thread
                Callable<Long> worker = new Engine(ps);
                //l'esito del thread
                Future<Long> submit = executor.submit(worker);
                //list.add(submit);
                list.put(pec, submit);
                
                
                
                
            } catch (ConfigLoaderException ex) {
                log.error("Il Thread non puo essere avviato" + ex);
            } catch (EngineException ex) {
                log.error("Il Thread è andato in errore " + ex);
            }
            
        }
        log.info("Immissione thread terminata");

        //chiedo il termine dell'esecutore al comppletamento dei th
        executor.shutdown();
        
        
        
        log.info("Esiti dei thread");


        //for (Future<Long> future : list) {
        for (Map.Entry<String, Future<Long>> entry : list.entrySet()) {
            String key = entry.getKey();
            Future<Long> future = entry.getValue();
            log.info("Analizzo il TH: " + key);
            try {
                log.debug(future.get());
            } catch (InterruptedException | ExecutionException e) {
                log.error(e.toString());
            }
        }
        NDC.pop();
    }
}
