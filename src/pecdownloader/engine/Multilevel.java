/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pecdownloader.engine;

import org.apache.commons.lang.StringUtils;
import java.util.Stack;

/**
 *
 * @author Riccardo
 */
class Multilevel {
    Stack<Integer> livelli= new Stack<>();

    void addLevel(int i) {
        livelli.push(new Integer(i));
    }

    void incLevel() {
        Integer valI = livelli.pop();
        int val =valI.intValue();
        val++;
        livelli.push(new Integer(val));
    }

    void removeLevel() {
        livelli.pop();
    }
    
    
    @Override
    public String toString(){
    return StringUtils.join(livelli,".");
}    
    
    
    
    
}
