/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pecdownloader.engine;

/**
 *
 * @author Riccardo
 */
import com.sun.mail.imap.IMAPNestedMessage;
import com.sun.mail.pop3.POP3SSLStore;
import com.sun.mail.imap.IMAPSSLStore;
import it.sgsbp.application.commons.configuration.ConfigLoader;
import it.sgsbp.application.commons.configuration.ConfigLoaderException;
import it.sgsbp.application.commons.fileUtils.TrivialFileUtils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.NDC;
import javax.mail.Session;
import javax.mail.URLName;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;
import org.apache.commons.io.IOUtils;
import pecdownloader.engine.settings.IMAPPECSettings;
import pecdownloader.engine.settings.PECSettingsInterface;
import pecdownloader.engine.settings.POPPECSettings;

public class Engine implements Callable<Long> {

    private final Logger log;
    private final PECSettingsInterface ps;
    private Multilevel ml;
    private String subject;

    Engine(PECSettingsInterface ps) throws EngineException {

        String uniqueLoggerName;
        try {
            uniqueLoggerName = ConfigLoader.getProperties("LOGGER_NAME");
        } catch (ConfigLoaderException ex) {
            throw new EngineException(ex.toString());
        }
        log = Logger.getLogger(uniqueLoggerName);

        this.ps = ps;



    }

    @Override
    public Long call() throws Exception {

        log.info(ps.getPECUid());
        NDC.push(this.getClass().getCanonicalName() + "  " + ps.getPECUid());
//         int a = RandomUtils.nextInt(20000);
//          log.debug("tempo di esecuzione: " + a);
//          Thread.sleep(a);
        if (ps instanceof POPPECSettings) {
            log.info("POP");
            doPOPWork(ps);
        } else if (ps instanceof IMAPPECSettings) {
            log.info("IMAP");
            doIMAPWork(ps);
        }


        NDC.pop();
        return RandomUtils.nextLong();
    }

    private void doPOPWork(PECSettingsInterface ps) {



        String POP = ps.getServer();
        log.debug("Server: " + POP);
        String POP_PORT = ps.getPort();
        log.debug("Porta: " + POP_PORT);
        String POP_PROTO = ps.getProto();
        log.debug("Protocollo: " + POP_PROTO);
        String username = ps.getUsername();
        log.debug("Username: " + username);
        String password = ps.getPassword();
        log.debug("Password: " + password);

        if (!POP_PROTO.isEmpty()) {
            try {
                log.debug("Protocollo: " + POP_PROTO);

                Properties pop3Props = new Properties();

                String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
                pop3Props.setProperty("mail.pop3.socketFactory.class", SSL_FACTORY);
                pop3Props.setProperty("mail.pop3.socketFactory.fallback", "false");
                pop3Props.setProperty("mail.pop3.port", POP_PORT);
                pop3Props.setProperty("mail.pop3.socketFactory.port", POP_PORT);

                URLName url = new URLName("pop3", POP, 995, "", username, password);
                Session session = Session.getInstance(pop3Props, null);
                POP3SSLStore store = new POP3SSLStore(session, url);

                log.info("Begin connection");

                store.connect();

                log.info("Connected");


                log.info("Elenco Cartelle");
                Folder[] f = store.getDefaultFolder().list();
                for (Folder fd : f) {
                    log.debug(">> " + fd.getName());
                }

                String POP_FOLDER = ps.getFolder();
                log.info("Cartella: " + POP_FOLDER);
                Folder folderInbox = store.getFolder(POP_FOLDER);
                folderInbox.open(Folder.READ_ONLY);

                Message[] arrayMessages = new Message[0];
                arrayMessages = folderInbox.getMessages();
                log.info("Trovati:" + arrayMessages.length);
//
//              //java.net.UnknownHostException
//
//
                store.close();
            } catch (MessagingException ex) {
                log.warn(ex.toString());
            }
        } else {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    private void doIMAPWork(PECSettingsInterface ps) {
        String IMAP = ps.getServer();
        log.debug("Server: " + IMAP);
        String IMAP_PORT = ps.getPort();
        log.debug("Porta: " + IMAP_PORT);
        String IMAP_PROTO = ps.getProto();
        log.debug("Protocollo: " + IMAP_PROTO);
        String username = ps.getUsername();
        log.debug("Username: " + username);
        String password = ps.getPassword();
        log.debug("Password: " + password);

        if (!IMAP_PROTO.isEmpty()) {
            try {
                log.debug("Protocollo: " + IMAP_PROTO);

                Properties imapProps = new Properties();

                String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
                imapProps.setProperty("mail.imap.socketFactory.class", SSL_FACTORY);
                imapProps.setProperty("mail.imap.socketFactory.fallback", "false");
                imapProps.setProperty("mail.imap.port", IMAP_PORT);
                imapProps.setProperty("mail.imap.socketFactory.port", IMAP_PORT);

                URLName url = new URLName("imap", IMAP, 993, "", username, password);
                Session session = Session.getInstance(imapProps, null);
                //IMAPPSSLStore store = new IMAPPSSLStore(session, url);
                IMAPSSLStore store = new IMAPSSLStore(session, url);
                log.info("Begin connection");

                store.connect();

                log.info("Connected");

                log.info("Elenco Cartelle");
                Folder[] f = store.getDefaultFolder().list();
                for (Folder fd : f) {
                    log.debug(">> " + fd.getName());
                }
                String IMAP_FOLDER = ps.getFolder();
                log.info("Cartella: " + IMAP_FOLDER);
                Folder folderInbox = store.getFolder(IMAP_FOLDER);
                folderInbox.open(Folder.READ_ONLY);

                //--------------------------------------------------------------
                //SEZIONE GENERALIZZABILE
                //--------------------------------------------------------------

                log.info("Trovati totali:" + folderInbox.getMessageCount());
                log.info("Trovati in cancellazione:" + folderInbox.getDeletedMessageCount());
                log.info("Trovati nuovi:" + folderInbox.getNewMessageCount());
                log.info("Trovati non letti:" + folderInbox.getUnreadMessageCount());

                /* Message messages[] = folderInbox.search(
                 new FlagTerm(new Flags(Flags.Flag.SEEN), false));
                 */

                //Passo tutti i messaggi
                Message messages[] = folderInbox.getMessages();
                log.info("Messaggi: " + messages.length);

                ml = new Multilevel();
                ml.addLevel(1);
                for (Message message : messages) {
                    log.info("Messaggio numero: " + ml.toString());
                    this.elaborateMessage(message);
                    ml.incLevel();
                }//for message



                /*if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                 log.info("la parte è un allegato");
                 String fileName = part.getFileName();
                 //part.saveFile(".\\backup"+File.separatorChar+ TrivialFileUtils.getValidFileName(subject)+File.separatorChar+fileName);
                 InputStream stream = part.getInputStream();
                 byte[] data = IOUtils.toByteArray(stream);
                 TrivialFileUtils.saveFile(  ".\\backup"+File.separatorChar+ TrivialFileUtils.getValidFileName(subject) ,
                 "",
                 fileName,
                 "",
                 "",
                 "",
                 false,
                 data
                 );
                                
                 } else {
                 log.info("La parte non è un allegato è altro");
                 //Object content = part.getContent();
                                
                 InputStream stream = part.getInputStream();
                               
                 byte[] data = IOUtils.toByteArray(stream);
                 TrivialFileUtils.saveFile(  ".\\backup"+File.separatorChar+ TrivialFileUtils.getValidFileName(subject) ,
                 "",
                 message.getSubject(),
                 "",
                 ".message",
                 "",
                 false,
                 data
                 );
                                

                 }
                 }
                 } else if (contentType.contains("text/plain")
                 || contentType.contains("text/html")) {
                 message.getInputStream();
                 InputStream stream = message.getInputStream();
                 byte[] data = IOUtils.toByteArray(stream);
                 TrivialFileUtils.saveFile(  ".\\backup"+File.separatorChar+ TrivialFileUtils.getValidFileName(subject) ,
                 "",
                 message.getSubject(),
                 "",
                 ".message",
                 "",
                 false,
                 data
                 );
                 }
                 */














//        javax.mail.Folder[] folders = store.getDefaultFolder().list("INBOX");
//        for (javax.mail.Folder folder : folders) {
//           /* if ((folder.getType() & javax.mail.Folder.HOLDS_MESSAGES) != 0) {
//                System.out.println(folder.getFullName() + ": " + folder.getMessageCount());
//            }*/
//            log.debug(">*> " + folder.getName());
//        }
//

//              //java.net.UnknownHostException
//
//  


                store.close();

            } catch (MessagingException ex) {
                log.warn(ex.toString());
            } catch (IOException ex) {
                log.warn(ex.toString());
            }
        } else {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    private void elaborateMessage(Message message) throws MessagingException, IOException {
        log.info("=======================================================================================");
        log.info("=======================================================================================");
        log.info("=======================================================================================");
        log.info("=======================================================================================");
        String xx = "ELABORAZIONE MESSAGGIO";
        log.info("Livello Elaborazione: " + ml.toString());
        Address[] froms = message.getFrom();
        log.info("From : " + froms[0]);
        String subject = message.getSubject();
        log.info("Subject : " + subject);
        Date sentDate = message.getSentDate();
        log.info("Send Date : " + sentDate.toString());
        String contentType = message.getContentType();
        log.info("Content Type : " + contentType);

        this.subject=subject;
        Object o = message.getContent();
        ml.addLevel(1);
        this.analizeContent(o);
        ml.removeLevel();


    }

    private void analizeContent(Object o) throws MessagingException, IOException {
        log.info("********************************");


        log.info("Livello Elaborazione: " + ml.toString());

        if (o instanceof MimeMultipart) {
            log.info("**This is a MimeMultipart Message**");
            MimeMultipart multipart = (MimeMultipart) o;
            int numberOfParts = multipart.getCount();
            log.info("Parti: " + numberOfParts);
            ml.addLevel(1);
            for (int i = 0; i < numberOfParts; i++) {
                //ottengo la parte
                MimeBodyPart part = (MimeBodyPart) multipart.getBodyPart(i);
                log.info("mimeType:" + part.getContentType());
                Object o2 = part.getContent();
                log.info("Subpart");
                this.analizeContent(o2);
                ml.incLevel();
            }//for multipart  
            ml.removeLevel();


        } else if (o instanceof String) {
            log.info("**This is a String Message**");
            byte[] data = ((String)o).getBytes();
            log.info("SALVO");
            TrivialFileUtils.saveFile(".\\backup" + File.separatorChar + TrivialFileUtils.getValidFileName(subject),
                    "",
                    ml.toString(),
                    "",
                    "",
                    "",
                    false,
                    data);
        } else if (o instanceof InputStream) {
            log.info("**This is an InputStream BodyPart**");
            
            byte[] data = IOUtils.toByteArray(((InputStream)o));
            log.info("SALVO");
            TrivialFileUtils.saveFile(".\\backup" + File.separatorChar + TrivialFileUtils.getValidFileName(subject),
                    "",
                    ml.toString(),
                    "",
                    "",
                    "",
                    false,
                    data);
            
        } else if (o instanceof IMAPNestedMessage ) {
           log.info("**This is an IMAPNestedMessage BodyPart**");
            InputStream stream = ((IMAPNestedMessage) o).getInputStream();
            Object o3=((IMAPNestedMessage) o).getContent();
            ml.addLevel(1);
            this.analizeContent(o3);
            ml.removeLevel();
            
           /* byte[] data = IOUtils.toByteArray(stream);
            log.info("SALVO");
            TrivialFileUtils.saveFile(".\\backup" + File.separatorChar + TrivialFileUtils.getValidFileName(subject),
                    "",
                    ml.toString(),
                    "",
                    "",
                    "",
                    false,
                    data);
            
           */

        } else {
            log.info("**This a unk OBJ**");
            log.info(o.getClass().getName());
            
        }



    }
}
